#include <iostream>
#include <string>

using namespace std;

class Animal
{
public:
	Animal() {}
	virtual void Voice()
	{
		cout << "Sample text" << "\n";
	}
	virtual ~Animal() {}
};

class Dog : public Animal
{
public:
	Dog() {}

	void Voice() override
	{
		cout << "Woof!" << "\n";
	}

	~Dog() {}
};

class Cat : public Animal
{
public:
	Cat() {}

	void Voice() override
	{
		cout << "Meow!" << "\n";
	}
	~Cat() {}
};

class Cow : public Animal
{
public:
	Cow() {}

	void Voice() override
	{
		cout << "Moo!" << "\n";
	}
	~Cow() {}
};

int main()
{
	int size = 3;
	Animal** array = new Animal*[size];
	Animal* Object_Dog = new Dog;
	Animal* Object_Cat = new Cat;
	Animal* Object_Cow = new Cow;
	array[0] = Object_Dog;
	array[1] = Object_Cat;
	array[2] = Object_Cow;

	for (int i = 0; i < size; i++)
	{
		array[i]->Voice();
	}
	
	delete Object_Dog;
	delete Object_Cat;
	delete Object_Cow;
	delete[] array;
}

